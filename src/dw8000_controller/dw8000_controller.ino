#include <MIDI.h>
#include <Bounce.h>
#include "dw8000_defines.h"
#include "MIDI_LED_indicator.h"

const int readResolution = DEFAULT_ANALOG_READ_RESOLUTION;
const int maxAnalogReadValue = (2 << (readResolution-1)) - 1;

int midiChannel = DEFAULT_MIDI_CHANNEL;

//Analog controls are mapped by [multiplexerChannel][pinIndex]
ControlPin analogControls[NUMBER_OF_MULTIPLEXER_CHANNELS][NUMBER_OF_MULTIPLEXERS];

//Variables used for sending the state of all controls to the MIDI OUT
Bounce controlDumpButton = Bounce(DIGITAL_INPUT_PIN_DUMP_CONTROLS_BUTTON, BUTTON_DEBOUNCE_DELAY);
bool forceSendControlSysex = false;

//We select waveform by using two 4-step rotary switches, so we need to keep their states.
int osc1WaveFormControlValue1 = 0;
int osc1WaveFormControlValue2 = 0;

int osc2WaveFormControlValue1 = 0;
int osc2WaveFormControlValue2 = 0;

MidiLedIndicator midiLedIndicator = MidiLedIndicator(MIDI_INDICATOR_LED);

MIDI_CREATE_DEFAULT_INSTANCE();

void handleMidiNoteOn(byte channel, byte note, byte velocity) {
  //Serial.println("Note on");
  midiLedIndicator.noteOn();
}

void handleMidiNoteOff(byte channel, byte note, byte velocity) {
  //Serial.println("Note off");
  midiLedIndicator.noteOff();
}

void handleMidiControlChange(byte channel, byte number, byte value) {
  midiLedIndicator.cc();
}

void setup() {
  initializePins();
  
  mapAnalogControlsToMultiplexers();
  addJitterThresholdToAnalogControls();
  analogReadAveraging(ANALOG_READ_AVERAGING);
  
  MIDI.setHandleNoteOn(handleMidiNoteOn);
  MIDI.setHandleNoteOff(handleMidiNoteOff);
  MIDI.setHandleControlChange(handleMidiControlChange);
  MIDI.begin(midiChannel);
  
  delay(200);
}

void loop() {
  readControlDumpButton();
  readAnalogControlInputsAndMidiIn();
  
  midiLedIndicator.update();
}

void readControlDumpButton() {
  if (controlDumpButton.update()) {
    forceSendControlSysex = controlDumpButton.fallingEdge();
  } else if (forceSendControlSysex) {
    forceSendControlSysex = false;
//    Serial.println("Controldump");
  }
}

void readAnalogControlInputsAndMidiIn() {
  for (int channel = 0; channel < NUMBER_OF_MULTIPLEXER_CHANNELS; channel++) {
    selectMultiplexerChannel(channel);
    readMultiplexersOnChannel(channel);
    MIDI.read();
  }
}

void selectMultiplexerChannel(int channel) {
   digitalWrite(MULTIPLEXER_CHANNEL_CONTROL_PIN_3, HIGH && (channel & B00000100));
   digitalWrite(MULTIPLEXER_CHANNEL_CONTROL_PIN_2, HIGH && (channel & B00000010));
   digitalWrite(MULTIPLEXER_CHANNEL_CONTROL_PIN_1, HIGH && (channel & B00000001));

  // allow signals to stablize
  delayMicroseconds(MUX_CHANNEL_SWITCH_DELAY_IN_MICROSECONDS);
}

void readMultiplexersOnChannel(int channel) {
  for (int multiplexer = 0; multiplexer < NUMBER_OF_MULTIPLEXERS; multiplexer++) {
    ControlPin& control = analogControls[channel][multiplexer];
    if (control.pin != -1) {
      readAnalogControlInput(control);
    } else {
      //Serial.println("NO PIN");
    }
  }
}

void readAnalogControlInput(struct ControlPin& control) {
  int readingFromAnalogRead = analogRead(control.pin);
  int processedValue = processReading(readingFromAnalogRead, control);

  sendSysExIfValueHasChanged(processedValue, control);
}

int processReading(int reading, struct ControlPin& control) {
  //The following prevents false value changes by checking if the high resolution reading difference is large enough.
  int value = control.previousValue;
  if (abs(reading-control.previousReading) > control.jitterThreshold) {
      control.previousReading = reading;
      value = map(reading, 0, maxAnalogReadValue+1, 0, control.maxValue+1);
  }
  
  return value;
}

void sendSysExIfValueHasChanged(int value, struct ControlPin& control) {
  if (forceSendControlSysex || value != control.previousValue) {

    control.previousValue = value;
    
    int param = control.param;
    
    if (param == PARAM_OSC_1_WAVEFORM_PART_1 || param == PARAM_OSC_1_WAVEFORM_PART_2) {
      if (param == PARAM_OSC_1_WAVEFORM_PART_1) {
        osc1WaveFormControlValue1 = value;
      } else {
        osc1WaveFormControlValue2 = value;
      }
      
      param = PARAM_OSC_1_WAVEFORM;
      value = calculateWaveformValueBasedOnTwoControls(osc1WaveFormControlValue1, osc1WaveFormControlValue2, 4);
    } else if (param == PARAM_OSC_2_WAVEFORM_PART_1 || param == PARAM_OSC_2_WAVEFORM_PART_2) {
      if (param == PARAM_OSC_2_WAVEFORM_PART_1) {
        osc2WaveFormControlValue1 = value;
      } else {
        osc2WaveFormControlValue2 = value;
      }
      
      param = PARAM_OSC_2_WAVEFORM;
      value = calculateWaveformValueBasedOnTwoControls(osc2WaveFormControlValue1, osc2WaveFormControlValue2, 4);
    } else if (param == PARAM_BEND_OSC) {
      switch (value) {
        case 0:
          value = 1;
          break;
        case 1:
          value = 2;
          break;
        case 2:
          value = 12;
          break;
        default:
          break;
      }
    }
    
    //Serial.println();
    //Serial.print("param: ");
    //Serial.println(param);
    //Serial.print("value: ");
    //Serial.println(value);
    
    createAndSendSysexMessage(param, value);
  }
}



int calculateWaveformValueBasedOnTwoControls(int controlValue1, int controlValue2, int numberOfControlSteps) {
  int waveformValue = numberOfControlSteps * controlValue1 + controlValue2;
    
  return waveformValue;
}

void createAndSendSysexMessage(uint8_t param, uint8_t value) {
//      Serial.print("Controlparam: ");
//      Serial.println(param);
//      Serial.print("Value: ");
//      Serial.println(value);
  MIDI.sendSysEx(NUMBER_OF_SYSEX_DIGITS, createSysExMessage(param, value), true);
}

const uint8_t* createSysExMessage(uint8_t param, uint8_t value) {
  static uint8_t data[NUMBER_OF_SYSEX_DIGITS] = {0xF0, 0x42, 0x0, 0x03, 0x41, 0x00, 0x00, 0xF7 };
  data[2] = 0x30 + midiChannel-1;
  data[5] = param;
  data[6] = value;

  return data;
}

void initializePins() {
    pinMode(MULTIPLEXER_CHANNEL_CONTROL_PIN_1, OUTPUT);
    pinMode(MULTIPLEXER_CHANNEL_CONTROL_PIN_2, OUTPUT);
    pinMode(MULTIPLEXER_CHANNEL_CONTROL_PIN_3, OUTPUT);
  
    pinMode(DIGITAL_INPUT_PIN_DUMP_CONTROLS_BUTTON, INPUT_PULLUP);
  
    analogReadResolution(DEFAULT_ANALOG_READ_RESOLUTION);
}

//Midi controls are mapped to [multiplexerChannel][multiplexerIndex]
void mapAnalogControlsToMultiplexers() {
////////******** MULTIPLEXER 1 ********////////
  
  analogControls[0][0] =  {ANALOG_INPUT_PIN_1, PARAM_OSC_1_OCTAVE,             2};
  analogControls[1][0] =  {ANALOG_INPUT_PIN_1, PARAM_OSC_1_WAVEFORM_PART_1,    3};
  analogControls[2][0] =  {ANALOG_INPUT_PIN_1, PARAM_OSC_1_WAVEFORM_PART_2,    3};
  analogControls[3][0] =  {ANALOG_INPUT_PIN_1, PARAM_OSC_1_LEVEL,              31};
  
  analogControls[4][0] =  {ANALOG_INPUT_PIN_1, PARAM_OSC_2_OCTAVE,             2};  
  analogControls[5][0] =  {ANALOG_INPUT_PIN_1, PARAM_OSC_2_WAVEFORM_PART_1,    3};
  analogControls[6][0] =  {ANALOG_INPUT_PIN_1, PARAM_OSC_2_WAVEFORM_PART_2,    3};
  analogControls[7][0] =  {ANALOG_INPUT_PIN_1, PARAM_OSC_2_INTERVAL,           4};

  
////////******** MULTIPLEXER 2 ********////////
  
  analogControls[0][1] =  {ANALOG_INPUT_PIN_2, PARAM_OSC_2_DETUNE,             6};
  analogControls[1][1] =  {ANALOG_INPUT_PIN_2, PARAM_OSC_2_LEVEL,              31};
  
  analogControls[2][1] =  {ANALOG_INPUT_PIN_2, PARAM_VCF_CUTOFF,               63};  
  analogControls[3][1] =  {ANALOG_INPUT_PIN_2, PARAM_VCF_RESONANCE,            31};
  analogControls[4][1] =  {ANALOG_INPUT_PIN_2, PARAM_VCF_KEYBOARD_TRACKING,    3};
  analogControls[5][1] =  {ANALOG_INPUT_PIN_2, PARAM_VCF_EG_POLARITY,          1};
  analogControls[6][1] =  {ANALOG_INPUT_PIN_2, PARAM_VCF_EG_INTENSITY,         31};
  
  analogControls[7][1] =  {ANALOG_INPUT_PIN_2, PARAM_MG_WAVEFORM,              3};
  
  
////////******** MULTIPLEXER 3 ********////////

  analogControls[0][2] =  {ANALOG_INPUT_PIN_3, PARAM_MG_FREQUENCY,             31};
  analogControls[1][2] =  {ANALOG_INPUT_PIN_3, PARAM_MG_DELAY,                 31};
  analogControls[2][2] =  {ANALOG_INPUT_PIN_3, PARAM_MG_OSC,                   31};
  analogControls[3][2] =  {ANALOG_INPUT_PIN_3, PARAM_MG_VCF,                   31};
  
  analogControls[4][2] =  {ANALOG_INPUT_PIN_3, PARAM_DELAY_TIME,               7};
  analogControls[5][2] =  {ANALOG_INPUT_PIN_3, PARAM_DELAY_FACTOR,             15};
  analogControls[6][2] =  {ANALOG_INPUT_PIN_3, PARAM_DELAY_FEEDBACK,           15};
  analogControls[7][2] =  {ANALOG_INPUT_PIN_3, PARAM_DELAY_FREQUENCY,          31};

  
////////******** MULTIPLEXER 4 ********////////

  analogControls[0][3] =  {ANALOG_INPUT_PIN_4, PARAM_DELAY_INTENSITY,          31};
  analogControls[1][3] =  {ANALOG_INPUT_PIN_4, PARAM_DELAY_EFFECT_LEVEL,       15};
  
  analogControls[2][3] =  {ANALOG_INPUT_PIN_4, PARAM_ASSIGN_MODE,              3};
  
  analogControls[3][3] =  {ANALOG_INPUT_PIN_4, PARAM_PORTAMENTO_TIME,          31};
  
  analogControls[4][3] =  {ANALOG_INPUT_PIN_4, PARAM_AFTERTOUCH_OSC_MG,        3};
  analogControls[5][3] =  {ANALOG_INPUT_PIN_4, PARAM_AFTERTOUCH_VCF,           3};
  analogControls[6][3] =  {ANALOG_INPUT_PIN_4, PARAM_AFTERTOUCH_VCA,           3};
  
  analogControls[7][3] =  {ANALOG_INPUT_PIN_4, PARAM_BEND_OSC,                 2};
  
  
////////******** MULTIPLEXER 5 ********////////

  analogControls[0][4] =  {ANALOG_INPUT_PIN_5, PARAM_BEND_VCF,                 1};
  
  analogControls[1][4] =  {ANALOG_INPUT_PIN_5, PARAM_VCA_EG_ATTACK,            31};
  analogControls[2][4] =  {ANALOG_INPUT_PIN_5, PARAM_VCA_EG_DECAY,             31};
  analogControls[3][4] =  {ANALOG_INPUT_PIN_5, PARAM_VCA_EG_BREAK_POINT_LEVEL, 31};
  analogControls[4][4] =  {ANALOG_INPUT_PIN_5, PARAM_VCA_EG_SLOPE,             31};
  analogControls[5][4] =  {ANALOG_INPUT_PIN_5, PARAM_VCA_EG_SUSTAIN,           31};
  analogControls[6][4] =  {ANALOG_INPUT_PIN_5, PARAM_VCA_EG_RELEASE,           31};
  analogControls[7][4] =  {ANALOG_INPUT_PIN_5, PARAM_VCA_EG_VELOCITY_SENS,     7};
    
  
////////******** MULTIPLEXER 6 ********////////

  analogControls[0][5] =  {ANALOG_INPUT_PIN_6, PARAM_VCF_EG_ATTACK,            31};
  analogControls[1][5] =  {ANALOG_INPUT_PIN_6, PARAM_VCF_EG_DECAY,             31};
  analogControls[2][5] =  {ANALOG_INPUT_PIN_6, PARAM_VCF_EG_BREAK_POINT_LEVEL, 31};
  analogControls[3][5] =  {ANALOG_INPUT_PIN_6, PARAM_VCF_EG_SLOPE,             31};
  analogControls[4][5] =  {ANALOG_INPUT_PIN_6, PARAM_VCF_EG_SUSTAIN,           31};
  analogControls[5][5] =  {ANALOG_INPUT_PIN_6, PARAM_VCF_EG_RELEASE,           31};
  analogControls[6][5] =  {ANALOG_INPUT_PIN_6, PARAM_VCF_EG_VELOCITY_SENS,     7};
  
  analogControls[7][5] =  {ANALOG_INPUT_PIN_6, PARAM_NOISE_LEVEL,              31};
      
  
////////******** MULTIPLEXER 7 ********////////

  analogControls[0][6] =  {ANALOG_INPUT_PIN_7, PARAM_AUTO_BEND_SELECT,         3};
  analogControls[1][6] =  {ANALOG_INPUT_PIN_7, PARAM_AUTO_BEND_MODE,           1};
  analogControls[2][6] =  {ANALOG_INPUT_PIN_7, PARAM_AUTO_BEND_TIME,           31};
  analogControls[3][6] =  {ANALOG_INPUT_PIN_7, PARAM_AUTO_BEND_INTENSITY,      31};
  analogControls[4][6] =  {-1}; //Empty slot
  analogControls[5][6] =  {-1}; //Empty slot
  analogControls[6][6] =  {-1}; //Empty slot
  analogControls[7][6] =  {-1}; //Empty slot
}

//Adds a threshold that controls how much a signal must differ to count as a value change.
void addJitterThresholdToAnalogControls() {
  for (int channel = 0; channel < NUMBER_OF_MULTIPLEXER_CHANNELS; channel++) {
    for (int multiplexer = 0; multiplexer < NUMBER_OF_MULTIPLEXERS; multiplexer++) {
      ControlPin& control = analogControls[channel][multiplexer];
      
      control.jitterThreshold = calculateJitterThreshold(control.maxValue);
    }
  }
}

int calculateJitterThreshold(int maxValue) {
  return (maxAnalogReadValue+1)/((DW8000_PARAM_MAX_VALUE+1) << 1);
}
