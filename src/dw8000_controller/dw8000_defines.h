#ifndef DW8000_defines_h
#define DW8000_defines_h

/*
DW-8000 sysex info:
F0 [Exclusive]
42 [KORG ID#]
3N [N=MIDI channel (N=0-F, Chan 1=0x00 Chan 16=0x0F)]
03 [DW-8000]
41 [Message "type"] (Parameter change = 0x41)
XX [Parameter number]
YY [Value (0-63)]
F7 [End of Exclusive]

Example:
A SysEx message for editing the VCF Resonance Parameter on MIDI channel 1:
F0 42 30 03 41 10 13 F7
*/

#define NUMBER_OF_SYSEX_DIGITS           8

//All the control parameter values the DW-8000 accepts

#define PARAM_OSC_1_OCTAVE               0  //0-2    0=16, 1=8, 2=4
#define PARAM_OSC_1_WAVEFORM             1  //0-15
#define PARAM_OSC_1_WAVEFORM_PART_1      1001 //0-3 We select waveform by using two 4-step rotary switches
#define PARAM_OSC_1_WAVEFORM_PART_2      2001 //0-3 We select waveform by using two 4-step rotary switches
#define PARAM_OSC_1_LEVEL                2  //0-31

#define PARAM_AUTO_BEND_SELECT           3  //0-3    0=OFF 1=OSC1 2=OSC2 3=BOTH
#define PARAM_AUTO_BEND_MODE             4  //0-1    0=UP  1=DOWN
#define PARAM_AUTO_BEND_TIME             5  //0-31
#define PARAM_AUTO_BEND_INTENSITY        6  //0-31

#define PARAM_OSC_2_OCTAVE               7  //0-2    0=16, 1=8, 2=4
#define PARAM_OSC_2_WAVEFORM             8  //0-15
#define PARAM_OSC_2_WAVEFORM_PART_1      1008 //0-3 We select waveform by using two 4-step rotary switches
#define PARAM_OSC_2_WAVEFORM_PART_2      2008 //0-3 We select waveform by using two 4-step rotary switches
#define PARAM_OSC_2_LEVEL                9  //0-31
#define PARAM_OSC_2_INTERVAL             10 //0-4
#define PARAM_OSC_2_DETUNE               11 //0-6

#define PARAM_NOISE_LEVEL                12 //0-31

#define PARAM_ASSIGN_MODE                13 //0-3    0=POLY1  1=POLY2  2=UNISON1  3=UNISON2
#define PARAM_PAR_NO_MEMO                14 //?

#define PARAM_VCF_CUTOFF                 15 //0-63
#define PARAM_VCF_RESONANCE              16 //0-31
#define PARAM_VCF_KEYBOARD_TRACKING      17 //0-3    0=0  1=1/4  2=1/2  3=1
#define PARAM_VCF_EG_POLARITY            18 //0-1    0=+  1=-
#define PARAM_VCF_EG_INTENSITY           19 //0-31

#define PARAM_VCF_EG_ATTACK              20 //0-31
#define PARAM_VCF_EG_DECAY               21 //0-31
#define PARAM_VCF_EG_BREAK_POINT_LEVEL   22 //0-31
#define PARAM_VCF_EG_SLOPE               23 //0-31
#define PARAM_VCF_EG_SUSTAIN             24 //0-31
#define PARAM_VCF_EG_RELEASE             25 //0-31
#define PARAM_VCF_EG_VELOCITY_SENS       26 //0-7

#define PARAM_VCA_EG_ATTACK              27 //0-31
#define PARAM_VCA_EG_DECAY               28 //0-31
#define PARAM_VCA_EG_BREAK_POINT_LEVEL   29 //0-31
#define PARAM_VCA_EG_SLOPE               30 //0-31
#define PARAM_VCA_EG_SUSTAIN             31 //0-31
#define PARAM_VCA_EG_RELEASE             32 //0-31
#define PARAM_VCA_EG_VELOCITY_SENS       33 //0-7

#define PARAM_MG_WAVEFORM                34 //0-3  0=Triangle  1=Saw  2=Ramp  3=Square
#define PARAM_MG_FREQUENCY               35 //0-31
#define PARAM_MG_DELAY                   36 //0-31
#define PARAM_MG_OSC                     37 //0-31
#define PARAM_MG_VCF                     38 //0-31

#define PARAM_BEND_OSC                   39 //0-12 (3 position switch, 1,2 and 12)
#define PARAM_BEND_VCF                   40 //0-1  0=OFF  1=ON

#define PARAM_DELAY_TIME                 41 //0-7
#define PARAM_DELAY_FACTOR               42 //0-15
#define PARAM_DELAY_FEEDBACK             43 //0-15
#define PARAM_DELAY_FREQUENCY            44 //0-31
#define PARAM_DELAY_INTENSITY            45 //0-31
#define PARAM_DELAY_EFFECT_LEVEL         46 //0-15

#define PARAM_PORTAMENTO_TIME            47 //0-31

#define PARAM_AFTERTOUCH_OSC_MG          48 //0-3
#define PARAM_AFTERTOUCH_VCF             49 //0-3
#define PARAM_AFTERTOUCH_VCA             50 //0-3

#define DW8000_PARAM_MAX_VALUE           63

//Multiplexers
#define NUMBER_OF_MULTIPLEXER_CHANNELS             8
#define NUMBER_OF_MULTIPLEXERS                     7

//Mapping pins for multiplexers
#define ANALOG_INPUT_PIN_1                         14
#define ANALOG_INPUT_PIN_2                         15
#define ANALOG_INPUT_PIN_3                         16
#define ANALOG_INPUT_PIN_4                         17
#define ANALOG_INPUT_PIN_5                         18
#define ANALOG_INPUT_PIN_6                         19
#define ANALOG_INPUT_PIN_7                         20

//These switches are not bound to actual parameters on the MKS-50. They provide additional funtionality.
#define DIGITAL_INPUT_PIN_DUMP_CONTROLS_BUTTON     11

#define MULTIPLEXER_CHANNEL_CONTROL_PIN_1          2
#define MULTIPLEXER_CHANNEL_CONTROL_PIN_2          3
#define MULTIPLEXER_CHANNEL_CONTROL_PIN_3          4

//Midi setup
#define DEFAULT_MIDI_CHANNEL                       1
#define MIDI_INDICATOR_LED                         13

//Sets analog read resolution to 10-bit (0-1023)
#define DEFAULT_ANALOG_READ_RESOLUTION             10

#define BUTTON_DEBOUNCE_DELAY                      200
//These can be modified if analog readings act unreliable
#define ANALOG_READ_AVERAGING                      10
#define MUX_CHANNEL_SWITCH_DELAY_IN_MICROSECONDS   100

struct ControlPin {
  int pin;
  int param;
  int maxValue;
  
  int jitterThreshold;
  
  int previousReading;
  int previousValue;
};

#endif
